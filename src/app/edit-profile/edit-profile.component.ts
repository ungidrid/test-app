import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.css']
})
export class EditProfileComponent implements OnInit {

  public moqData = {
    sex: 0,
    name: 'John doe',
    email: 'email@email.com',
    dob: '2020-06-13'
  };

  constructor() { }

  ngOnInit() {
  }

}
