import { BrowserModule } from '@angular/platform-browser';
import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';

import { AppComponent } from './app.component';
import {OnsenModule} from "ngx-onsenui";
import {FormsModule} from "@angular/forms";
import {TogglePasswordVisibilityDirective} from "./toggle-password-visibility/toggle-password-visibility.directive";
import { LogInPageComponent } from './log-in-page/log-in-page.component';
import {RouterModule} from "@angular/router";
import { AppRoutingModule } from './app-routing.module';
import { EditProfileComponent } from './edit-profile/edit-profile.component';

@NgModule({
  declarations: [
    AppComponent,
    TogglePasswordVisibilityDirective,
    LogInPageComponent,
    EditProfileComponent
  ],
    imports: [
        BrowserModule,
        OnsenModule,
        FormsModule,
        RouterModule,
        AppRoutingModule,
    ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
