import {Directive, ElementRef} from '@angular/core';

@Directive({
  selector: '[toggleVisibility]'
})
export class TogglePasswordVisibilityDirective {
  private _shown = false;

  constructor(private el: ElementRef) {
    const parent = this.el.nativeElement.parentNode;
    parent.removeChild(this.el.nativeElement);

    const divWithToggle = document.createElement('div');
    divWithToggle.style.position = 'relative';

    const span = document.createElement('span');
    span.className = 'fa fa-fw fa-eye field-icon';
    span.addEventListener('click', () => {
      this.toggle(span);
    });
    divWithToggle.appendChild(this.el.nativeElement);
    divWithToggle.appendChild(span);
    parent.appendChild(divWithToggle);
  }

  toggle(span: HTMLElement) {
    this._shown = !this._shown;
    if (this._shown) {
      this.el.nativeElement.setAttribute('type', 'text');
      span.classList.remove('fa-eye');
      span.classList.add('fa-eye-slash');
    } else {
      this.el.nativeElement.setAttribute('type', 'password');
      span.classList.remove('fa-eye-slash');
      span.classList.add('fa-eye');
    }
  }
}
